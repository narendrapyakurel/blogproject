from django.contrib import admin
from .models import *

admin.site.register(Blog)
admin.site.register(Event)
admin.site.register(Category)
admin.site.register(News)
admin.site.register(Message)