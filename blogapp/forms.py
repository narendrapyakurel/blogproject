from django import forms
from .models import *
from django.contrib.auth.models import User

class BlogForm(forms.ModelForm):
	class Meta:
		model=Blog
		fields=['title','content','author','image']

class NewsForm(forms.ModelForm):
	class Meta:
		model=News
		fields=['title','category','image','detail','author']

class MessageForm(forms.Form):
	sender=forms.CharField(widget=forms.TextInput())
	mobile=forms.CharField(widget=forms.NumberInput())
	email=forms.CharField(widget=forms.EmailInput())
	subject=forms.CharField(widget=forms.TextInput())
	message=forms.CharField(widget=forms.Textarea())
			
class LoginForm(forms.Form):
	username=forms.CharField(widget=forms.TextInput())
	password=forms.CharField(widget=forms.PasswordInput())

class UserForm(forms.Form):
	username=forms.CharField(widget=forms.TextInput())
	email=forms.CharField(widget=forms.EmailInput())
	password=forms.CharField(widget=forms.PasswordInput())
	confirm_password=forms.CharField(widget=forms.PasswordInput())
	def clean_confirm_password(self):
		password=self.cleaned_data.get('password')
		confirm_password=self.cleaned_data.get('confirm_password')
		if password != confirm_password:
			raise forms.ValidationError('password & confirm password didnot match')
		return confirm_password
	def clean_username(self):
		username=self.cleaned_data.get('username')
		users=User.objects.filter(username=username)
		if users.exists():
			raise forms.ValidationError('username is  already created')
		return username
		i