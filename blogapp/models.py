from django.db import models
class Blog(models.Model):
	title = models.CharField(max_length=200)
	image = models.ImageField(upload_to="blogs")
	content = models.TextField()
	author = models.CharField(max_length=100)
	date = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.title+"("+self.author+")"+str(self.date)

class Event(models.Model):
	name = models.CharField(max_length=100)
	content = models.TextField()
	composed = models.CharField(max_length=100)
	etype = models.CharField(max_length=100)
	edate = models.DateField()

	def __str__(self):
		return self.composed + " " + str(self.edate)

class Category(models.Model):
	title=models.CharField(max_length=200)
	image=models.ImageField(upload_to='category')

	def __str__(self):
		return self.title
class News(models.Model):
	title=models.CharField(max_length=200)
	category=models.ForeignKey(Category,on_delete=models.CASCADE,related_name='relatednews')
	# category=models.ManyToManyField(Category)
	image=models.ImageField(upload_to='news')
	detail=models.TextField()
	date=models.DateTimeField(auto_now_add=True)
	author=models.CharField(max_length=100,null=True,blank=True)

	def __str__(self):
		return self.title

class Message(models.Model):
	name=models.CharField(max_length=200)
	phone=models.CharField(max_length=200)
	email=models.EmailField(null=True,blank=True)
	message=models.CharField(max_length=100)
	subject=models.CharField(max_length=100)
	date=models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.name