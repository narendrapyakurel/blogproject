from django.urls import path
from .views import *
app_name="blogapp"
urlpatterns=[
path('',HomeView.as_view(),name='home'),
path('about/',AboutView.as_view(),name='about'),
path('contact/',ContactView.as_view(),name='contact'),
path('nepal/',NepalView.as_view(),name='nepal'),
path('blog/list/',BlogListView.as_view(),name='bloglist'),
path("blog/<int:pk>/detail/",BlogDetailView.as_view(),name='blogdetail'),
path('news/list/',NewsListView.as_view(),name='newslist'),
path("news/<int:pk>/detail/",NewsDetailView.as_view(),name='newsdetail'),
path("blog/create/",BlogCreateView.as_view(),name='blogcreate'),
path("blog/<int:pk>/update/",BlogUpdateView.as_view(),name='updateview'),
path("blog/<int:pk>/delete/",BlogDeleteView.as_view(),name='deleteview'),
path('news/create/',NewsCreateView.as_view(),name='newscreate'),
path('news/<int:pk>/update/',NewsUpdateView.as_view(),name='nupdateview'),
path('news/<int:pk>/delete/',NewsDeleteView.as_view(),name='ndeleteview'),
path('login/',LoginView.as_view(),name='login'),
path('logout/',LogoutView.as_view(),name='logout'),
path('category/<int:pk>/',CategoryDetailView.as_view(),name='categorydetail'),
path('userform/',UserRegisterForm.as_view(),name='userforms'),

]