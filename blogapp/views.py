from django.views.generic import *
from django.shortcuts import render,redirect
from .models import *
from .forms import *
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User

# Create your views here.
class HomeView(TemplateView):
	template_name="home.html"

	def get_context_data(self,**kwargs):
		context=super().get_context_data(**kwargs)
		context['narendra']='i am narendra'
		context['blogs']=Blog.objects.all()
		context['events']=Event.objects.all()
		context['categorylist']=Category.objects.all()
		return context

class  AboutView(TemplateView):
	template_name='about.html'

class ContactView(FormView):
	template_name='contact.html' 
	form_class=MessageForm
	success_url="/"

	def form_valid(self,form):
		sender=form.cleaned_data["sender"]
		mobile=form.cleaned_data['mobile']
		email=form.cleaned_data['email']
		subject=form.cleaned_data['subject']
		message=form.cleaned_data['message']
		print(sender,mobile,email,subject,message)
		Message.objects.create(name=sender,phone=mobile,email=email,subject=subject,message=message)
		return super().form_valid(form)         

class NepalView(TemplateView):
	template_name='nepal.html'
		
class BlogListView(ListView):
	template_name='bloglist.html'
	queryset=Blog.objects.all()
	# model=Blog
	context_object_name='allblogs'

class EventListView(ListView):
	template_name='enventlist.html'
	queryset=Event.objects.all()
	# model=Blog
	context_object_name='allevents'

class BlogDetailView(DetailView):
	template_name='blogdetail.html'
	model=Blog
	context_object_name='blog'

class NewsListView(ListView):
	template_name='newslist.html'
	queryset=News.objects.all()
	context_object_name='allnews'

class NewsDetailView(DetailView):
	template_name="newsdetail.html" 
	model=News
	context_object_name='news'

class NewsCreateView(CreateView):
	template_name='newscreate.html'
	form_class=NewsForm
	success_url="/"

class NewsUpdateView(UpdateView):
	template_name='newscreate.html'
	form_class=NewsForm
	success_url='/news/list'
	model=News

class NewsDeleteView(DeleteView):
	template_name='newsdelete.html'
	model=News
	success_url="/news/list/"

class BlogCreateView(LoginRequiredMixin,CreateView):
	template_name='blogcreate.html'
	form_class=BlogForm
	success_url="/"
	login_url="/login/"

	def get_context_data(self,**kwargs):
		context=super().get_context_data(**kwargs)
		context['blogs']=Blog.objects.all()
		return context


	def form_valid(self,form):
		title=form.cleaned_data['title']
		print(title)
		return super().form_valid(form)  


class BlogUpdateView(LoginRequiredMixin,UpdateView):
	template_name='blogcreate.html'
	form_class=BlogForm
	success_url="/blog/list"
	model=Blog
	login_url="/login/"

class BlogDeleteView(LoginRequiredMixin,DeleteView):
	template_name="blogdelete.html"
	model=Blog
	success_url="/blog/list/"
	login_url="/login/"


class LoginView(FormView):
	template_name='login.html'
	form_class=LoginForm
	success_url="/"
	def form_valid(self,form):
		uname=form.cleaned_data['username']
		pword=form.cleaned_data['password']
		user=authenticate(username=uname,password=pword)
		if user is not None:
			login(self.request,user)
		else:
			return render(self.request,self.template_name,{'form':form,'error':"you are not authorized"})
		print(uname,pword,"-----------------")
		return super().form_valid(form)

class LogoutView(View):
	def get(self,request):
		logout(request)
		return redirect('/')

class CategoryDetailView(DetailView):
	template_name='categorydetail.html'
	model=Category
	context_object_name='category'
class UserRegisterForm(FormView):
	template_name='usercreate.html'
	form_class=UserForm
	success_url="/"
	def form_valid(self,form):
		username=form.cleaned_data['username']
		email=form.cleaned_data['email']
		password=form.cleaned_data['password']
		print(username)
		User.objects.create_user(username,email,password)
		return super().form_valid(form)
